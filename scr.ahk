﻿::shrugman::¯\_(ツ)_/¯

; Ctrl+/, for lines through letters and sometimes IPA or other things
^/::
input, command, L1 ; L1 to limit the input to 3 keys.
if ( command == "A" )
    Send Æ
else if ( command == "a" )
    Send æ
else if ( command == "C" )
    Send ©
else if ( command == "c" )
    Send ¢
else if ( command == "D" )
    Send Ð
else if ( command == "d" )
    Send ð
else if ( command == "E" )
    Send €
else if ( command == "e" )
    Send ə
else if ( command == "g" )
    Send ɡ
else if ( command == "H" )
    Send Ħ
else if ( command == "h" )
    Send ħ
else if ( command == "I" )
    Send İ
else if ( command == "i" )
    Send ı
else if ( command == "j" )
    Send ȷ
else if ( command == "L" )
    Send Ł
else if ( command == "l" )
    Send ł
else if ( command == "N" )
    Send Ŋ
else if ( command == "n" )
    Send ŋ
else if ( command == "O" )
    Send Œ
else if ( command == "o" )
    Send œ
else if ( command == "P" )
    Send ¶
else if ( command == "p" )
	Send §
else if ( command == "R" )
    Send ®
else if ( command == "S" )
    Send ß
else if ( command == "s" )
    Send ʃ
else if ( command == "T" )
    Send Þ
else if ( command == "t" )
    Send þ
else if ( command == "U" )
    Send Ø
else if ( command == "u" )
    Send ø
else if ( command == "W" )
    Send Ƿ
else if ( command == "w" )
    Send ƿ
else if ( command == "Y" )
    Send Ȝ
else if ( command == "y" )
    Send ȝ
else if ( command == "z" )
    send ʒ
else if ( command == ":" )
    Send ː
else if ( command == "'" )
    send ˈ
else if ( command == "?" )
    Send ʔ
else if ( command == "." )
    Send · ; Interpunct
else if ( command == "!" )
	Send ‽
return

; Apostrophe, mostly for acute accents
^'::
input, command, L1 ;
if ( command == " " )
    Send % Chr(0x0301) ; Combining Acute Accent
else if ( command == "A" )
    Send Á
else if ( command == "a" )
    Send á
else if ( command == "C" )
    Send Ć
else if ( command == "c" )
    Send ć
else if ( command == "E" )
    Send É
else if ( command == "e" )
    Send é
else if ( command == "G" )
    Send Ǵ
else if ( command == "g" )
    Send ǵ
else if ( command == "I" )
    Send Í
else if ( command == "i" )
    Send í
else if ( command == "K" )
    Send Ḱ
else if ( command == "k" )
    Send ḱ
else if ( command == "M" )
    Send Ḿ
else if ( command == "m" )
    Send ḿ
else if ( command == "N" )
    Send Ñ
else if ( command == "n" )
    Send ñ
else if ( command == "O" )
    Send Ó
else if ( command == "o" )
    Send ó
else if ( command == "P" )
    Send Ṕ
else if ( command == "p" )
    Send ṕ
else if ( command == "U" )
    Send Ú
else if ( command == "u" )
    Send ú
else if ( command == "W" )
    Send Ẃ
else if ( command == "w" )
    Send ẃ
else if ( command == "Y" )
    Send Ý
else if ( command == "y" )
    Send ý
else if ( command == "!" )
    Send ¡
else if ( command == "?" )
    Send ¿
return

; Double acutes
^+'::
input, command, L1
if ( command == " " )
    Send % Chr(0x030B) ; Combining Double Acute Accent
else if ( command == "O" )
    send Ő
else if ( command == "o")
    Send ő
else if ( command == "U" )
    send Ű
else if ( command == "u" )
    send ű
return

; Grave, mostly for grave accents
^`::
input, command, L1 ;
if ( command == " " )
    Send % Chr(0x0300) ; Combining Grave Accent
else if ( command == "A" )
    Send À
else if ( command == "a" )
    Send à
else if ( command == "E" )
    Send È
else if ( command == "e" )
    Send è
else if ( command == "I" )
    Send Ì
else if ( command == "i" )
    Send ì
else if ( command == "O" )
    Send Ò
else if ( command == "o" )
    Send ò
else if ( command == "U" )
    Send Ù
else if ( command == "u" )
    Send ù
else if ( command == "W" )
    Send Ẁ
else if ( command == "w" )
    Send ẁ
else if ( command == "Y" )
    Send Ỳ
else if ( command == "y" )
    Send ỳ
return

; Tildes
^+`::
input, command, L1 ;
if ( command == " " )
    Send % Chr(0x0303) ; Combining Tilde
else if ( command == "A" )
    Send Ã
else if ( command == "a" )
    Send ã
else if ( command == "E" )
    Send Ẽ
else if ( command == "e" )
    Send ẽ
else if ( command == "I" )
    Send Ĩ
else if ( command == "i" )
    Send ĩ
else if ( command == "l" )
    Send ɫ
else if ( command == "N" )
    Send Ñ
else if ( command == "n" )
    Send ñ
else if ( command == "O" )
    Send Õ
else if ( command == "o" )
    Send õ
else if ( command == "U" )
    Send Ũ
else if ( command == "u" )
    Send ũ
else if ( command == "V" )
    Send Ṽ
else if ( command == "v" )
    Send ṽ
return

; Umlauts/Diaereses
^+;::
input, command, L1 ;
if ( command == " " )
    Send % Chr(0x0308) ; Combining Diaeresis
else if ( command == "A" )
    Send Ä
else if ( command == "a" )
    Send ä
else if ( command == "E" )
    Send Ë
else if ( command == "e" )
    Send ë
else if ( command == "H" )
    Send Ḧ
else if ( command == "h" )
    Send ḧ
else if ( command == "I" )
    Send Ï
else if ( command == "i" )
    Send ï
else if ( command == "O" )
    Send Ö
else if ( command == "o" )
    Send ö
else if ( command == "t" )
    Send ẗ
else if ( command == "U" )
    Send Ü
else if ( command == "u" )
    Send ü
else if ( command == "W" )
    Send Ẅ
else if ( command == "w" )
    send ẅ
else if ( command == "X" )
    Send Ẍ
else if ( command == "x" )
    Send ẍ
else if ( command == "Y" )
    Send Ÿ
else if ( command == "y" )
    Send ÿ
return

; Rings, IPA with loops
^+2::
input, command, L1 ;
if ( command == " " )
    Send % Chr(0x030A) ; Combining Ring Above
else if ( command == "A" )
    Send Å
else if ( command == "a" )
    Send å
else if ( command == "c" )
    Send ɕ
else if ( command == "d" )
    Send ȡ
else if ( command == "j" )
    Send ʝ
else if ( command == "l" )
    Send ȴ
else if ( command == "n" )
    Send ȵ
else if ( command == "o" )
    Send ⱺ
else if ( command == "t" )
    Send ȶ
else if ( command == "U" )
    Send Ů
else if ( command == "u" )
    Send ů
else if ( command == "v" )
    Send ⱴ
else if ( command == "w" )
    Send ẘ
else if ( command == "x" )
    Send ɣ
else if ( command == "y" )
    Send ẙ
else if ( command == "z" )
    Send ʑ
return

; Circumflexes
^+6::
input, command, L1 ;
if ( command == " " )
    Send % Chr(0x0302) ; Combining Circumflex Accent (above)
else if ( command == "A" )
    Send Â
else if ( command == "a" )
    Send â
else if ( command == "C" )
    Send Ĉ
else if ( command == "c" )
    Send ĉ
else if ( command == "E" )
    Send Ê
else if ( command == "e" )
    Send ê
else if ( command == "G" )
    Send Ĝ
else if ( command == "g" )
    Send ĝ
else if ( command == "I" )
    Send Î
else if ( command == "i" )
    Send î
else if ( command == "J" )
    Send Ĵ
else if ( command == "j" )
    Send ĵ
else if ( command == "O" )
    Send Ô
else if ( command == "o" )
    Send ô
else if ( command == "S" )
    Send Ŝ
else if ( command == "s" )
    Send ŝ
else if ( command == "U" )
    Send Û
else if ( command == "u" )
    Send û
else if ( command == "W" )
    Send Ŵ
else if ( command == "w" )
    Send ŵ
else if ( command == "Y" )
    Send Ŷ
else if ( command == "y" )
    Send ŷ
else if ( command == "Z" )
    Send Ẑ
else if ( command == "z" )
    Send ẑ
return


; Cedillas (NOT OGONEKS)
^,::
input, command, L1 ;
if ( command == " " )
    Send % Chr(0x0327) ; Combining cedilla below
else if ( command == "C" )
    Send Ç
else if ( command == "c" )
    Send ç
else if ( command == "E" )
    Send Ȩ
else if ( command == "e" )
    Send ȩ
else if ( command == "G" )
    Send Ģ
else if ( command == "g" )
    Send ģ
else if ( command == "K" )
    Send Ķ
else if ( command == "k" )
    Send ķ
else if ( command == "L" )
    Send Ļ
else if ( command == "l" )
    Send ļ
else if ( command == "N" )
    Send Ņ
else if ( command == "n" )
    Send ņ
else if ( command == "R" )
    Send Ŗ
else if ( command == "r" )
    Send ŗ
else if ( command == "S" )
    Send Ș
else if ( command == "s" )
    Send ș
else if ( command == "T" )
    Send Ț
else if ( command == "t" )
    Send ț
return

; Ogonek (NOT CEDILLAS)
^<::
input, command, L1 ;
if ( command == " " )
    Send % Chr(0x0328) ; Combining ogonek below
else if ( command == "A" )
    Send Ą
else if ( command == "a" )
    Send ą
else if ( command == "E" )
    Send Ę
else if ( command == "e" )
    Send ę
else if ( command == "I" )
    Send Į
else if ( command == "i" )
    Send į
else if ( command == "O" )
    Send Ǫ
else if ( command == "o" )
    Send ǫ
else if ( command == "U" )
    Send Ų
else if ( command == "u" )
    Send ų
return

; Dot below
^+.::
input, command, L1 ;
if ( command == " " )
    Send % Chr(0x0323) ; Combining dot below
else if ( command == "A" )
    Send Ạ
else if ( command == "a" )
    Send ạ
else if ( command == "B" )
    Send Ḅ
else if ( command == "b" )
    Send ḅ
else if ( command == "D" )
    Send Ḍ
else if ( command == "d" )
    Send ḍ
else if ( command == "E" )
    Send Ẹ
else if ( command == "e" )
    Send ẹ
else if ( command == "H" )
    Send Ḥ
else if ( command == "h" )
    Send ḥ
else if ( command == "I" )
    Send Ị
else if ( command == "i" )
    Send ị
else if ( command == "K" )
    Send Ḳ
else if ( command == "k" )
    Send ḳ
else if ( command == "L" )
    Send Ḷ
else if ( command == "l" )
    Send ḷ
else if ( command == "M" )
    Send Ṃ
else if ( command == "m" )
    Send ṃ
else if ( command == "N" )
    Send Ṇ
else if ( command == "n" )
    Send ṇ
else if ( command == "O" )
    Send Ọ
else if ( command == "o" )
    Send ọ
else if ( command == "R" )
    Send Ṛ
else if ( command == "r" )
    Send ṛ
else if ( command == "S" )
    Send Ṣ
else if ( command == "s" )
    Send ṣ
else if ( command == "T" )
    Send Ṭ
else if ( command == "t" )
    Send ṭ
else if ( command == "U" )
    Send Ụ
else if ( command == "u" )
    Send ụ
else if ( command == "V" )
    Send Ṿ
else if ( command == "v" )
    Send ṿ
else if ( command == "W" )
    Send Ẉ
else if ( command == "w" )
    Send ẉ
else if ( command == "Y" )
    Send Ỵ
else if ( command == "y" )
    Send ỵ
else if ( command == "Z" )
    Send Ẓ
else if ( command == "z" )
    Send ẓ
return

; Dot above
^.::
input, command, L1 ;
if ( command == " " )
    Send % Chr(0x0307) ; Combining dot above
else if ( command == "A" )
    Send Ȧ
else if ( command == "a" )
    Send ȧ
else if ( command == "B" )
    Send Ḃ
else if ( command == "b" )
    Send ḃ
else if ( command == "C" )
    Send Ċ
else if ( command == "c" )
    Send ċ
else if ( command == "D" )
    Send Ḋ
else if ( command == "d" )
    Send ḋ
else if ( command == "E" )
    Send Ė
else if ( command == "e" )
    Send ė
else if ( command == "F" )
    Send Ḟ
else if ( command == "f" )
    Send ḟ
else if ( command == "G" )
    Send Ġ
else if ( command == "g" )
    Send ġ
else if ( command == "H" )
    Send Ḣ
else if ( command == "h" )
    Send ḣ
else if ( command == "I" )
    Send İ
else if ( command == "i" )
    Send ı
else if ( command == "L" )
    Send Ŀ
else if ( command == "l" )
    Send ŀ
else if ( command == "M" )
    Send Ṁ
else if ( command == "m" )
    Send ṁ
else if ( command == "N" )
    Send Ṅ
else if ( command == "n" )
    Send ṅ
else if ( command == "O" )
    Send Ȯ
else if ( command == "o" )
    Send ȯ
else if ( command == "P" )
    Send Ṗ
else if ( command == "p" )
    Send ṗ
else if ( command == "R" )
    Send Ṙ
else if ( command == "r" )
    Send ṙ
else if ( command == "S" )
    Send Ṡ
else if ( command == "s" )
    Send ṡ
else if ( command == "T" )
    Send Ṫ
else if ( command == "t" )
    Send ṫ
else if ( command == "W" )
    Send Ẇ
else if ( command == "w" )
    Send ẇ
else if ( command == "X" )
    Send Ẋ
else if ( command == "x" )
    Send ẋ
else if ( command == "Y" )
    Send Ẏ
else if ( command == "y" )
    Send ẏ
else if ( command == "Z" )
    Send Ż
else if ( command == "z" )
    Send ż
return
    
; Macrons
; ĀāǢǣĒēḠḡĪīḴḵḺḻṈṉŌōṞṟṮṯŪūȲȳẔẕ
^+-::
input, command, L1 ;
if ( command == " " )
    Send % Chr(0x0304) ; Combining macron
else if ( command == "A" )
    Send Ā
else if ( command == "a" )
    Send ā
else if ( command == "B" )
    Send Ḇ
else if ( command == "b" )
    Send ḇ
else if ( command == "D" )
    Send Ḏ
else if ( command == "d" )
    Send ḏ
else if ( command == "E" )
    Send Ē
else if ( command == "e" )
    Send ē
else if ( command == "G" )
    Send Ḡ
else if ( command == "g" )
    Send ḡ
else if ( command == "I" )
    Send Ī
else if ( command == "i" )
    Send ī
else if ( command == "K" )
    Send Ḵ
else if ( command == "k" )
    Send ḵ
else if ( command == "L" )
    Send Ḻ
else if ( command == "l" )
    Send ḻ
else if ( command == "N" )
    Send Ṉ
else if ( command == "n" )
    Send ṉ
else if ( command == "O" )
    Send Ō
else if ( command == "o" )
    Send ō
else if ( command == "R" )
    Send Ṟ
else if ( command == "r" )
    Send ṟ
else if ( command == "T" )
    Send Ṯ
else if ( command == "t" )
    Send ṯ
else if ( command == "U" )
    Send Ū
else if ( command == "u" )
    Send ū
else if ( command == "Y" )
    Send Ȳ
else if ( command == "y" )
    Send ȳ
else if ( command == "Z" )
    Send Ẕ
else if ( command == "z" )
    Send ẕ
return

; Carons/Haceks
^+7::
input, command, L1 ;
if ( command == " " )
    Send % Chr(0x030C) ; Combining caron
else if ( command == "A" )
    Send Ǎ
else if ( command == "a" )
    Send ǎ
else if ( command == "C" )
    Send Č
else if ( command == "c" )
    Send č
else if ( command == "D" )
	Send Ď
else if ( command == "d" )
    Send ď
else if ( command == "E" )
    Send Ě
else if ( command == "e" )
    Send ě
else if ( command == "G" )
    Send Ǧ
else if ( command == "g" )
    Send ǧ
else if ( command == "H" )
    Send Ȟ
else if ( command == "h" )
    Send ȟ
else if ( command == "I" )
    Send Ǐ
else if ( command == "i" )
    Send ǐ
else if ( command == "K" )
    Send Ǩ
else if ( command == "k" )
    Send ǩ
else if ( command == "N" )
    Send Ň
else if ( command == "n" )
    Send ň
else if ( command == "O" )
    Send Ǒ
else if ( command == "o" )
    Send ǒ
else if ( command == "R" )
    Send Ř
else if ( command == "r" )
    Send ř
else if ( command == "S" )
    Send Š
else if ( command == "s" )
    Send š
else if ( command == "T" )
    Send Ť
else if ( command == "t" )
    Send ť
else if ( command == "U" )
    Send Ǔ
else if ( command == "u" )
    Send ǔ
else if ( command == "Z" )
    Send Ž
else if ( command == "z" )
    Send ž
return

; Still to do: Much (most?) of IPA

; Three-letter commands: Ligatures, Math
^+3::
input, command, L3 ;
if ( command == "   " )
    Send ☃
else if ( command == "2rt" )
    Send √
else if ( command == "3rt" )
    Send ∛
else if ( command == "4rt" )
    Send ∜
else if ( command == "AE " )
	Send Æ
else if ( command == "AE'" )
	Send Ǽ
else if ( command == "AE-" )
	Send Ǣ
else if ( command == "ae " )
	Send æ
else if ( command == "ae'" )
	Send ǽ
else if ( command == "ae-" )
	Send ǣ
else if ( command == "ang" )
	Send ∠
else if ( command == "And" )
	Send ∩
else if ( command == "and" )
	Send ∧
else if ( command == "apx" )
    Send ≈
else if ( command == "crs" )
    Send ×
else if ( command == "Dag" )
	Send ‡
else if ( command == "dag" )
	Send †
else if ( command == "del" )
    Send ∆
else if ( command == "div" )
    Send ∂
else if ( command == "dvd" )
    Send ÷
else if ( command == "DZ " )
	Send Ǳ
else if ( command == "DZh" )
	Send Ǆ
else if ( command == "Dz " )
	Send ǲ
else if ( command == "Dzh" )
	Send ǅ
else if ( command == "dz " )
	Send ǳ
else if ( command == "dz2" )
	Send ʣ
else if ( command == "dzh" )
	Send ǆ
else if ( command == "dzj" )
	Send ʤ
else if ( command == "dzc" )
	Send ʥ
else if ( command == "ell" )
    Send ℓ
else if ( command == "gte" )
    Send ≥
else if ( command == "ff " )
    Send ﬀ
else if ( command == "ffi" )
    Send ﬃ
else if ( command == "ffl" )
    Send ﬄ
else if ( command == "fi " )
    Send ﬁ
else if ( command == "fl " )
    Send ﬂ
else if ( command == "fng" )
	Send ʩ
else if ( command == "ide" )
    Send ≡
else if ( command == "IJ " )
	Send Ĳ
else if ( command == "ij " )
	Send ĳ
else if ( command == "inf" )
    Send ∞
else if ( command == "LJ " )
	Send Ǉ
else if ( command == "Lj " )
	Send ǈ
else if ( command == "lj " )
	Send ǉ
else if ( command == "ls " )
	Send ʪ
else if ( command == "lte" )
    Send ≤
else if ( command == "lz " )
	Send ʫ
else if ( command == "lzh" )
	Send ɮ
else if ( command == "mpl" )
    Send ∓
else if ( command == "nab" )
    Send ∇
else if ( command == "neq" )
    Send ≠
else if ( command == "NJ " )
	Send Ǌ
else if ( command == "Nj " )
	Send ǋ
else if ( command == "nj " )
	Send ǌ
else if ( command == "not" )
    Send ¬   
else if ( command == "OE " )
	Send Œ
else if ( command == "oe " )
	Send œ
else if ( command == "Or " )
	Send ∪
else if ( command == "or " )
	Send ∨
else if ( command == "plm" )
    Send ±
else if ( command == "prd" )
    Send ∏
else if ( command == "st " )
    Send ﬆ
else if ( command == "st2" )
	Send ﬅ
else if ( command == "sum" )
    Send ∑
else if ( command == "ts " )
	Send ʦ
else if ( command == "ts2" )
	Send ʧ
else if ( command == "tcc" )
	Send ʨ
return


; Greeeeeek
^+4::
input, command, L1 ;
if ( command == "A" )
    Send Α
else if ( command == "a" )
    Send α
else if ( command == "B" )
	Send Β
else if ( command == "b" )
	Send β
else if ( command == "C" )
    Send Χ
else if ( command == "c" )
    Send χ
else if ( command == "D" )
	Send Δ
else if ( command == "d" )
    Send δ
else if ( command == "E" )
    Send Ε
else if ( command == "e" )
    Send ε
else if ( command == "F" )
    Send Φ
else if ( command == "f" )
    Send φ
else if ( command == "G" )
    Send Γ
else if ( command == "g" )
    Send γ
else if ( command == "H" )
    Send Η
else if ( command == "h" )
    Send η
else if ( command == "I" )
    Send Ι
else if ( command == "i" )
    Send ι
else if ( command == "K" )
    Send Κ
else if ( command == "k" )
    Send κ
else if ( command == "L" )
    Send Λ
else if ( command == "l" )
    Send λ
else if ( command == "M" )
    Send Μ
else if ( command == "m" )
    Send μ
else if ( command == "N" )
    Send Ν
else if ( command == "n" )
    Send ν
else if ( command == "O" )
    Send Ο
else if ( command == "o" )
    Send ο
else if ( command == "P" )
    Send Π
else if ( command == "p" )
    Send π
else if ( command == "Q" )
    Send Θ
else if ( command == "q" )
    Send θ
else if ( command == "R" )
    Send Ρ
else if ( command == "r" )
    Send ρ
else if ( command == "S" )
    Send Σ
else if ( command == "s" )
    Send σ
else if ( command == "T" )
    Send Τ
else if ( command == "t" )
    Send τ
else if ( command == "U" )
    Send Υ
else if ( command == "u" )
    Send υ
else if ( command == "V" )
    Send Ψ
else if ( command == "v" )
    Send ψ
else if ( command == "W" )
    Send Ω
else if ( command == "w" )
    Send ω
else if ( command == "X" )
    Send Ξ
else if ( command == "x" )
    Send ξ
else if ( command == "Z" )
    Send Ζ
else if ( command == "z" )
    Send ζ
return


; Five-letter commands: IPA
^+1::
input, command, L5 ;
if ( command == "     " )
    Send ☃
; PULMONIC CONSONANTS:
; NASALS:
else if ( command == "ualna" ) ; Unvoiced alveolar nasal
	Send n̥
else if ( command == "ubina" ) ; Unvoiced bilabial nasal
	Send m̥
else if ( command == "upana" ) ; Unvoiced palatal nasal
	Send ɲ̊
else if ( command == "urena" ) ; Unvoiced retroflex nasal
	Send ɳ̊
else if ( command == "uuvna" ) ; Unvoiced uvular nasal
	Send ɴ̥
else if ( command == "uvena" ) ; Unvoiced velar nasal
	Send ŋ̊
else if ( command == "valna" ) ; Voiced alveolar nasal
	Send n
else if ( command == "vbina" ) ; Voiced bilabial nasal
	Send m
else if ( command == "vlana" ) ; Voiced labiodental nasal
	Send ɱ
else if ( command == "vpana" ) ; Voiced palatal nasal
	Send ɲ
else if ( command == "vrena" ) ; Voiced retroflex nasal
	Send ɳ
else if ( command == "vuvna" ) ; Voiced uvular nasal
	Send ɴ
else if ( command == "vvena" ) ; Voiced velar nasal
	Send ŋ
return
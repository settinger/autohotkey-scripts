# Keyboard Shortcut Compiler
# Converts tables and things into long AHK-readable scripts

out = ''
script = []

# §1 One-character input/output
# §1.1 Ctrl+/
script.append(['; Ctrl+/\n; Odds and ends, relatively-commonly used symbols',
             '^/',
             '{space}',
             "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz:'?.!",
             "Ææ  ©¢Ðð€ə   ɡĦħİı ȷ  Łł  ŊŋŒœ¶§  ® ßʃÞþØø  Ƿƿ  Ȝȝ ʒːˈʔ·‽"])
# §1.2 Ctrl+' acutes
script.append(["; Ctrl+'\n; Acute accents, Spanish punctuation",
               "^'",
               "% Chr(0x0301) ; Combining Acute Accent",
               "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz !?",
               "Áá  Ćć  Éé  Ǵǵ  Íí  Ḱḱ  ḾḿÑñÓóṔṕ        Úú  Ẃẃ  Ýý   ¡¿"])
# §1.3 Ctrl+` graves
script.append(["; Ctrl+`\n; Grave accents",
               "^`",
               "% Chr(0x0300) ; Combining Grave Accent",
               "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz",
               "Àà      Èè      Ìì          Òò          Ùù  Ẁẁ  Ỳỳ  "])
# §1.4 Ctrl+" double acutes
script.append(['; Ctrl+"\n; Double acute accents',
               "^+'",
               "% Chr(0x030B) ; Combining Double Acute Accent",
               "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz",
               "                            Őő          Űű"])
# §1.5 Ctrl+~ tildes above or inside letters
script.append(["; Ctrl+~\n; Tildes",
               "^+`",
               "% Chr(0x0303) ; Combining Tilde",
               "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz",
               "Ãã      Ẽẽ      Ĩĩ     ɫ  ÑñÕõ          ŨũṼṽ"])
# §1.6 Ctrl+: umlauts/diaereses
script.append(["; Ctrl+:\n; Umlauts and diaereses",
               "^+;",
               "% Chr(0x0308) ; Combining Diaeresis",
               "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz",
               "Ää      Ëë    ḦḧÏï          Öö         ẗÜü  ẄẅẌẍŸÿ"])
# §1.7 Ctrl+@ rings and loops
script.append(["; Ctrl+@\n; Rings and loops",
               "^+2",
               "% Chr(0x030A) ; Combining Ring Above",
               "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz",
               "Åå   ɕ ȡ           ʝ   ȴ   ȵ ⱺ         ȶŮů ⱴ ẘ ɣ ẙ ʑ"])
# §1.7 Ctrl+^ circumflex
script.append(["; Ctrl+^\n; Circumflexes",
               "^+6",
               "% Chr(0x0302) ; Combining Circumflex Accent (above)",
               "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz",
               "Ââ  Ĉĉ  Êê  Ĝĝ  ÎîĴĵ        Ôô      Ŝŝ  Ûû  Ŵŵ  ŶŷẐẑ"])
# §1.7 Ctrl+& caron/hacek
script.append(["; Ctrl+&\n; Caron/hacek",
               "^+7",
               "% Chr(0x030C) ; Combining caron",
               "AaBbCcDdEeFfGgHhIiJjKkLlMmNnOoPpQqRrSsTtUuVvWwXxYyZz",
               "Ǎǎ  ČčĎďĚě  ǦǧȞȟǏǐ  Ǩǩ    ŇňǑǒ    ŘřŠšŤťǓǔ        Žž"])
# §1.8 Ctrl+, cedillas


form1 = '\
{}\n\
{}::\n\
input, command, L1 ;\n\
if ( command == " " )\n\
    Send {}\n'
form2 = '\
else if ( command == "{}" )\n\
    Send {}\n'
form3 = '\
else\n\
    Send {space}\n\
return\n\n'


# Get number of lines in 1char.txt


for block in script:
    out += form1.format(block[0], block[1], block[2])
    for roman, char in zip(block[3],block[4]):
        if char != ' ':
            out += form2.format(roman, char)
    out += form3

# §2 Three character input, one character output
# Maybe put this in a different file?
# §3 Five character input, one character output
# Maybe put this in a different file?
